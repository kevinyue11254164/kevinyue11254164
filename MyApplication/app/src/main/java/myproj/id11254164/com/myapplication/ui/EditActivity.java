package myproj.id11254164.com.myapplication.ui;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import myproj.id11254164.com.myapplication.R;
import myproj.id11254164.com.myapplication.adapters.DrawerLayoutAdapter;
import myproj.id11254164.com.myapplication.database.DBContentProvider;
import myproj.id11254164.com.myapplication.util.Constants;
import myproj.id11254164.com.myapplication.util.Integers;

public class EditActivity extends AppCompatActivity {


    private String mAction;
    private EditText mTitleEditor;
    private EditText mEditor;
    private TextView mLocationTv;
    private TextView mDateTv;
    private String mNewText;
    private String mNoteFilter;
    private String mOldText;
    private String mDate;
    private String mLocation;
    private String mNewTitle;
    private String mOldTitle;
    private String mFontSize;
    private String mFontColor;
    private boolean mFontBold;
    private boolean mFontItalics;

    private static final int MAP_REQUEST_CODE = 100;

    private android.support.v4.app.ActionBarDrawerToggle mDrawerListener;
    private DrawerLayout mDrawerLayout;
    private ListView mListView;
    private DrawerLayoutAdapter mDrawAdapter;
    ShareDialog mShareDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        FacebookSdk.sdkInitialize(getApplicationContext());
        mShareDialog = new ShareDialog(this);

//Finding the TextViews and EditText
        mDateTv = (TextView) findViewById(R.id.DateSelected);
        mLocationTv = (TextView) findViewById(R.id.LocationSelected);
        mEditor = (EditText) findViewById(R.id.editText);
        mTitleEditor = (EditText) findViewById(R.id.editText_Title);
        mListView = (ListView) findViewById(R.id.drawerList);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(EditActivity.this, MyPrefernceActivity.class));
            }
        });

        //Working with DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawAdapter = new DrawerLayoutAdapter(this);
        mListView.setAdapter(mDrawAdapter);

        mDrawerListener = new android.support.v4.app.ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_action_action_list, R.string.open_drawer, R.string.drawer_close) {

            //When drawer is Opened, close Keyboard
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            //When drawer is close, close Keyboard
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                InputMethodManager inputMethodManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

        };

        mDrawerLayout.setDrawerListener(mDrawerListener);


        //Getting data from MainActivity
        //Storing, Updating the text
        Intent intent = getIntent();

        Uri uri = intent.getParcelableExtra(DBContentProvider.CONTENT_ITEM_TYPE);
        if (uri == null) {
            mAction = Intent.ACTION_INSERT;
            setTitle(getString(R.string.new_note));
        } else {
            mAction = Intent.ACTION_EDIT;
            mNoteFilter = Constants.NOTE_ID + "=" + uri.getLastPathSegment();

            Cursor cursor = getContentResolver().query(
                    uri, Constants.ALL_COLUMNS, mNoteFilter, null, null);
            cursor.moveToFirst();

            mDate = cursor.getString(cursor.getColumnIndex(Constants.NOTE_CREATED));
            mOldText = cursor.getString(cursor.getColumnIndex(Constants.NOTE_TEXT));
            mOldTitle = cursor.getString(cursor.getColumnIndex(Constants.NOTE_TITLE));
            mLocation = cursor.getString(cursor.getColumnIndex(Constants.NOTE_LOCATION));

            mDate = mDate.substring(Integers.INT_ZERO, Integers.INT_TEN);
            mLocationTv.setText(mLocation);
            mDateTv.setText(mDate);
            mTitleEditor.setText(mOldTitle);
            mEditor.setText(mOldText);
            mEditor.requestFocus();
        }
    }

    /**
     * Updates Database when called
     * Updates the noteTitle, noteText
     */
    private void submit() {

        mNewText = mEditor.getText().toString().trim();
        mLocation = mLocationTv.getText().toString().trim();
        mNewTitle = mTitleEditor.getText().toString().trim();
        switch (mAction) {
            case Intent.ACTION_INSERT:
                if (mNewText.length() == Integers.INT_ZERO) {
                    setResult(RESULT_CANCELED);
                } else {
                    insertNote(mNewText, mNewTitle, mLocation);

                    Log.d(Constants.LOG_D, mLocation + " " + mNewText + " " + mNewTitle);
                    break;
                }
            case Intent.ACTION_EDIT:
                if (mNewText.length() == Integers.INT_ZERO) {
                    deleteNote();
                } else if (mOldText.equals(mNewText)) {
                    setResult(RESULT_CANCELED);
                } else {
                    updateNote(mNewText, mNewTitle, mLocation);
                    Log.d(Constants.LOG_D, mLocation + " " + mNewText + " " + mNewTitle);
                }
        }
        finish();

    }

    public void addLocation(View view) {
        Log.d(Constants.LOG_D, Constants.INTENT_ONE);

        Intent intent = new Intent(this, MapsActivity.class);
        startActivityForResult(intent, MAP_REQUEST_CODE);
    }

    /**
     * Get requestCode from Activities, mainly Google Maps
     * Update the Location textView
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(Constants.LOG_D, requestCode + getString(R.string.activityResult) + " " + resultCode);

        if (requestCode == MAP_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                mLocation = data.getStringExtra(Constants.INTENT_ONE);
                mLocationTv.setText(mLocation);
                mLocationTv.requestFocus();

                Log.d(Constants.LOG_D, mLocation + getString(R.string.activityResult));

            }
            if (resultCode == RESULT_CANCELED) {
                mLocationTv.setText(R.string.noneSelected);
            }
        }
    }

    /**
     * onResume called for when Text Preferences has run
     * updates Text to display the certain style chosen
     */
    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mFontSize = sp.getString(Constants.PREF_FONT_SIZE, getString(R.string.ten));
        mFontColor = sp.getString(Constants.PREF_FONT_COLOR, getString(R.string.black));
        mFontBold = sp.getBoolean(Constants.PREF_FONT_BOLD, false);
        mFontItalics = sp.getBoolean(Constants.PREF_FONT_ITALICS, false);

        Float mFont = Float.valueOf(mFontSize);
        mEditor.setTextSize(mFont);
        setColor(mFontColor);

        if (mFontBold == true && mFontItalics == false) {
            mEditor.setTypeface(null, Typeface.BOLD);
        } else if (mFontItalics == true && mFontBold == false) {
            mEditor.setTypeface(null, Typeface.ITALIC);
        } else if (mFontBold == true && mFontItalics == true) {
            mEditor.setTypeface(null, Typeface.BOLD_ITALIC);
        } else {
            mEditor.setTypeface(null, Typeface.NORMAL);
        }


    }

    /**
     * Simply updates the note
     * And sends it back to the MainActivity to display
     *
     * @param noteText
     * @param noteTitle
     * @param noteLocation
     */
    private void updateNote(String noteText, String noteTitle, String noteLocation) {
        ContentValues values = new ContentValues();
        values.put(Constants.NOTE_TEXT, noteText);
        values.put(Constants.NOTE_TITLE, noteTitle);
        values.put(Constants.NOTE_LOCATION, noteLocation);
        getContentResolver().update(DBContentProvider.CONTENT_URI, values, mNoteFilter, null);
        Toast.makeText(this, R.string.note_updated, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
    }

    /**
     * Inserts a new note.
     *
     * @param noteText
     * @param noteTitle
     * @param noteLocation
     */
    private void insertNote(String noteText, String noteTitle, String noteLocation) {
        ContentValues values = new ContentValues();
        values.put(Constants.NOTE_TITLE, noteTitle);
        values.put(Constants.NOTE_TEXT, noteText);
        values.put(Constants.NOTE_LOCATION, noteLocation);
        getContentResolver().insert(DBContentProvider.CONTENT_URI, values);
        setResult(RESULT_OK);
    }

    /**
     * When back pressed run submit()
     * Will check if the text is empty
     * If so, will delete the whole note
     */
    @Override
    public void onBackPressed() {
        submit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mAction.equals(Intent.ACTION_EDIT)) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_edit, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                submit();
                break;
            case R.id.action_delete_single:
                deleteNote();
        }
        return true;
    }

    /**
     * Deletes note when called
     */
    private void deleteNote() {
        getContentResolver().delete(DBContentProvider.CONTENT_URI, mNoteFilter, null);
        Toast.makeText(this, R.string.note_deleted, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Setting font color
     *
     * @param color
     */
    private void setColor(String color) {
        switch (color) {
            case "Red":
                mEditor.setTextColor(Color.RED);
                break;
            case "Green":
                mEditor.setTextColor(Color.GREEN);
                break;
            case "Blue":
                mEditor.setTextColor(Color.BLUE);
                break;
            case "Black":
                mEditor.setTextColor(Color.BLACK);
                break;
            default:
                mEditor.setTextColor(Color.BLACK);
        }
    }

    /**
     * For the Facebook API
     * Shares the content that is currently being written
     *
     * @param view
     */
    public void shareContent(View view) {

        mNewText = mEditor.getText().toString().trim();
        mNewTitle = mTitleEditor.getText().toString().trim();

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(mNewTitle)
                    .setContentDescription(
                            mNewText)
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build();

            mShareDialog.show(linkContent);

            Log.d(Constants.LOG_D, mNewText + " " + mNewTitle);
        }
    }

}
