package myproj.id11254164.com.myapplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import myproj.id11254164.com.myapplication.R;


public class DrawerLayoutAdapter extends BaseAdapter {

    private TextView mDrawSList;
    private ImageView mDrawSImage;
    private Context mContext;
    private String[] mOptions;
    private int[] mImages = {R.drawable.ic_action_image_edit,
            R.drawable.ic_action_editor_format_color_text,
            R.drawable.ic_action_editor_format_bold, R.drawable.ic_action_editor_format_italic};

    public DrawerLayoutAdapter(Context context) {
        this.mContext = context;
        mOptions = context.getResources().getStringArray(R.array.Options);
    }

    @Override
    public int getCount() {
        return mOptions.length;
    }

    @Override
    public Object getItem(int position) {
        return mOptions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mRow = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mRow = inflater.inflate(R.layout.list_drawlayout, parent, false);
        } else {
            mRow = convertView;
        }
        mDrawSList = (TextView) mRow.findViewById(R.id.textView_singleDraw);
        mDrawSImage = (ImageView) mRow.findViewById(R.id.imageView_singleDraw);
        mDrawSList.setText(mOptions[position]);
        mDrawSImage.setImageResource(mImages[position]);

        return mRow;
    }
}
