package myproj.id11254164.com.myapplication.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import myproj.id11254164.com.myapplication.util.Constants;


public class DBOpenHelper extends SQLiteOpenHelper {

    //Constants for db name and version
    private static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 3;

    //SQL for table
    private static final String TABLE_CREATE =
            "CREATE TABLE " + Constants.TABLE_NOTES + " (" +
                    Constants.NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Constants.NOTE_TEXT + " TEXT, " +
                    Constants.NOTE_TITLE + " TEXT, " +
                    Constants.NOTE_LOCATION + " TEXT, " +
                    Constants.NOTE_CREATED + " TEXT default CURRENT_TIMESTAMP" +
                    ")";

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    /**
     * Recreates DB if already existed
     * Creates new one
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NOTES);
        onCreate(db);
    }
}
