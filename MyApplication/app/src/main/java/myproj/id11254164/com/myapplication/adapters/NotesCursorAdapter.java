package myproj.id11254164.com.myapplication.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myproj.id11254164.com.myapplication.R;
import myproj.id11254164.com.myapplication.util.Constants;
import myproj.id11254164.com.myapplication.util.Integers;

public class NotesCursorAdapter extends android.widget.CursorAdapter {

    private String mNoteDate;
    private String mNoteTitle;


    public NotesCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(
                R.layout.list_item, parent, false
        );

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        mNoteDate = cursor.getString(
                cursor.getColumnIndex(Constants.NOTE_CREATED));
        mNoteTitle = cursor.getString(
                cursor.getColumnIndex(Constants.NOTE_TITLE));

        mNoteDate = mNoteDate.substring(Integers.INT_ZERO, Integers.INT_TEN);

        TextView tvDate = (TextView) view.findViewById(R.id.tvNote);
        TextView tvTitle = (TextView) view.findViewById(R.id.tvDate);
        tvDate.setText(mNoteDate);
        tvTitle.setText(mNoteTitle);

    }
}
