package myproj.id11254164.com.myapplication.ui;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Toast;

import myproj.id11254164.com.myapplication.R;
import myproj.id11254164.com.myapplication.adapters.NotesCursorAdapter;
import myproj.id11254164.com.myapplication.database.DBContentProvider;
import myproj.id11254164.com.myapplication.util.Constants;
import myproj.id11254164.com.myapplication.util.Integers;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int EDIT_REQUEST_CODE = 169;
    private static final int REQUEST_FACEBOOK = 269;
    private CursorAdapter mCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCursorAdapter = new NotesCursorAdapter(this, null, Integers.INT_ZERO);

        ListView list = (ListView) findViewById(android.R.id.list);
        list.setAdapter(mCursorAdapter);
// Send intent when clicked on the adapter, retrieve the correct db
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, EditActivity.class);
                Uri uri = Uri.parse(DBContentProvider.CONTENT_URI + "/" + id);
                intent.putExtra(DBContentProvider.CONTENT_ITEM_TYPE, uri);
                startActivityForResult(intent, EDIT_REQUEST_CODE);
            }
        });

        getLoaderManager().initLoader(Integers.INT_ZERO, null, this);
    }

    /**
     * Inserts Note
     *
     * @param noteText
     * @param noteTitle
     */
    private void insertNote(String noteText, String noteTitle) {
        ContentValues values = new ContentValues();
        values.put(Constants.NOTE_TEXT, noteText);
        values.put(Constants.NOTE_TITLE, noteTitle);
        Uri noteUri = getContentResolver().insert(DBContentProvider.CONTENT_URI, values);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Options Item selected.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_create_sample:
                Intent intent = new Intent(this, EditActivity.class);
                startActivityForResult(intent, EDIT_REQUEST_CODE);
                break;
            case R.id.action_delete_all:
                deleteAllNotes();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Deletes all notes that are in the DB
     */
    private void deleteAllNotes() {

        DialogInterface.OnClickListener dialogClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int button) {
                        if (button == DialogInterface.BUTTON_POSITIVE) {
                            //Insert Data management code here
                            getContentResolver().delete(
                                    DBContentProvider.CONTENT_URI, null, null);
                            restartLoader();
                            Toast.makeText(MainActivity.this,
                                    getString(R.string.all_deleted),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                };
// Alert is popped up asking if the user REALLY wants to delete all
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.are_you_sure))
                .setPositiveButton(getString(android.R.string.yes), dialogClickListener)
                .setNegativeButton(getString(android.R.string.no), dialogClickListener)
                .show();

    }

    //Restarts loader to see updated database
    private void restartLoader() {
        getLoaderManager().restartLoader(0, null, this);
    }

    //Called whenever data is needed
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, DBContentProvider.CONTENT_URI,
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorAdapter.swapCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }

    public void addNewNote(View view) {
        Intent intent = new Intent(this, EditActivity.class);
        startActivityForResult(intent, EDIT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_REQUEST_CODE && resultCode == RESULT_OK) {
            restartLoader();
        }
    }

    public void goFacebook(View view) {
        Intent intent = new Intent(this, FacebookActivty.class);
        startActivityForResult(intent, REQUEST_FACEBOOK);
    }

}
