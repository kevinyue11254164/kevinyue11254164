package myproj.id11254164.com.myapplication.util;

public class Constants {

    //Constants for identifying table and columns
    public static final String TABLE_NOTES = "notes";
    public static final String NOTE_ID = "_id";
    public static final String NOTE_TEXT = "noteText";
    public static final String NOTE_TITLE = "noteTitle";
    public static final String NOTE_CREATED = "noteCreated";
    public static final String NOTE_LOCATION = "noteLocation";

    //Constants for Log.d
    public static final String LOG_D = "LogMeNow";
    public static final String TEST_STRING = "Testing";

    //Constants for text
    public static final String INTENT_ONE = "intentOne";

    public static final String[] ALL_COLUMNS =
            {NOTE_ID, NOTE_TEXT, NOTE_TITLE, NOTE_LOCATION, NOTE_CREATED};

    public static final String[] TITLES_TAB =
            {"IMAGE", "DIARY", "LOCATION"};

    public static final String PREF_FONT_SIZE = "fontSize";
    public static final String PREF_FONT_COLOR = "fontColor";
    public static final String PREF_LINE_SPACING = "lineSpacing";
    public static final String PREF_FONT_BOLD = "fontBold";
    public static final String PREF_FONT_ITALICS = "fontItalics";
}
