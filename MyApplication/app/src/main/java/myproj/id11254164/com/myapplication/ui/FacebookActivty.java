package myproj.id11254164.com.myapplication.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import myproj.id11254164.com.myapplication.R;
import myproj.id11254164.com.myapplication.util.Constants;

public class FacebookActivty extends AppCompatActivity {

    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private ProfilePictureView mProfImage;
    private SharedPreferences mSharedPref;
    private String mUserId;
    private String mUserIdNew;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_facebook_activty);
//calls the login button
        loginButton = (LoginButton) findViewById(R.id.login_button);
        mProfImage = (ProfilePictureView) findViewById(R.id.imgView_Prof);

        loginButton.setPublishPermissions("publish_actions");
//when user calls Login, run callbackManager
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            //onSuccess, save the userId into a preference
            @Override
            public void onSuccess(LoginResult loginResult) {
                mSharedPref = getSharedPreferences("userId", Context.MODE_PRIVATE);
                SharedPreferences.Editor mEditor = mSharedPref.edit();
                mUserId = loginResult.getAccessToken().getUserId();
                mEditor.putString("userName", mUserId);
                mProfImage.setProfileId(mUserId);
                Log.d(Constants.LOG_D, mUserId);
            }

            @Override
            public void onCancel() {
                Log.d(Constants.LOG_D, getString(R.string.cancelledHappen));
            }

            @Override
            public void onError(FacebookException e) {
                Log.d(Constants.LOG_D, getString(R.string.errorOccured));
            }
        });

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void displayData() {
        SharedPreferences sharedPreferences = getSharedPreferences("userId", Context.MODE_PRIVATE);
        mUserIdNew = sharedPreferences.getString("userName", "");
    }


}
