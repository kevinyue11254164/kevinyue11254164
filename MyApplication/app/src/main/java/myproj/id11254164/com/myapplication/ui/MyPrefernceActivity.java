package myproj.id11254164.com.myapplication.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import myproj.id11254164.com.myapplication.R;
import myproj.id11254164.com.myapplication.util.Constants;

public class MyPrefernceActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {

    private SharedPreferences mSharedPrefer;
    private ListPreference mFontSize;
    private ListPreference mFontColor;
    private CheckBoxPreference mFontBold;
    private CheckBoxPreference mFontItalics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mFontSize = (ListPreference) findPreference(Constants.PREF_FONT_SIZE);
        mFontColor = (ListPreference) findPreference(Constants.PREF_FONT_COLOR);
        mFontBold = (CheckBoxPreference) findPreference(Constants.PREF_FONT_BOLD);
        mFontBold = (CheckBoxPreference) findPreference(Constants.PREF_FONT_ITALICS);

    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mFontColor) {
            mFontColor.setSummary(newValue.toString());
        } else if (preference == mFontSize) {
            mFontSize.setSummary(newValue.toString());
        }
        return false;
    }
}
